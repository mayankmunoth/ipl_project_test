package com.mayank.ipl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    public static final int MATCH_ID = 0;
    public static final int INNING = 1;
    public static final int BATTING_TEAM = 2;
    public static final int BOWLING_TEAM = 3;
    public static final int OVER = 4;
    public static final int BALL = 5;
    public static final int BATSMAN = 6;
    public static final int NON_STRIKER = 7;
    public static final int BOWLER = 8;
    public static final int IS_SUPER_OVER = 9;
    public static final int WIDE_RUNS = 10;
    public static final int BYE_RUNS = 11;
    public static final int LEGBYE_RUNS = 12;
    public static final int NO_BALL_RUNS = 13;
    public static final int PANELTY_RUNS = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int PLAYER_DISMISSED = 18;
    public static final int DISMISSAL_KIND = 19;
    public static final int FIELDER = 20;

    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM_1 = 4;
    public static final int TEAM_2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUNS = 11;
    public static final int WIN_BY_WICKETS = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int UMPIRE_1 = 15;
    public static final int UMPIRE_2 = 16;
    public static final int UMPIRE_3 = 17;

    public static void main(String[] args) throws IOException {
        List<Match> matches = getMatchesData();
        List<Delivery> deliveries = getDeliveriesData();

        findNumberOfMatchesPlayed(matches);
        findNumberOfMatchesWonPerTeam(matches);
        findExtraRunsConcededPerTeamIn2016(matches, deliveries);
        findTheMostEconomicalBowlerIn2015(matches, deliveries);
        findMatchesPlayedOnEveryVenue(matches);
    }

    public static Map<Integer, Integer> findNumberOfMatchesPlayed(List<Match> matches) {
        TreeMap<Integer, Integer> everySessionMatchPlayed = new TreeMap<>();
        try {
            for (Match match : matches) {
                if (everySessionMatchPlayed.containsKey(match.getSeason())) {
                    everySessionMatchPlayed.put(match.getSeason(), everySessionMatchPlayed.get(match.getSeason()) + 1);
                } else {
                    everySessionMatchPlayed.put(match.getSeason(), 1);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return everySessionMatchPlayed;
    }

    public static Map<String,Integer> findNumberOfMatchesWonPerTeam(List<Match> matches) {
        HashMap<String, Integer> everyTeamWonMatches = new HashMap<>();

        try {
            for (Match match : matches) {
                if (everyTeamWonMatches.containsKey(match.getWinner())) {
                    everyTeamWonMatches.put(match.getWinner(), everyTeamWonMatches.get(match.getWinner()) + 1);
                } else {
                    everyTeamWonMatches.put(match.getWinner(), 1);
                }
            }
            everyTeamWonMatches.remove("", 3);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return everyTeamWonMatches;
    }


    public static Map<String, Integer> findExtraRunsConcededPerTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        HashMap<String, Integer> everyTeamCondactExtraRuns = new HashMap<>();

        try {
            List<Integer> matchesId = new ArrayList<>();
            for (Match match : matches) {
                if (match.getSeason() == 2016) {
                    matchesId.add(match.getId());
                }
            }

            for (int i = 0; i < matchesId.size(); i++) {
                for (Delivery delivery : deliveries) {

                    if (matchesId.get(i) == delivery.getMatchId()) {

                        if (everyTeamCondactExtraRuns.containsKey(delivery.getBattingTeam())) {
                            everyTeamCondactExtraRuns.put(delivery.getBattingTeam(), everyTeamCondactExtraRuns.get(delivery.getBattingTeam()) + delivery.getExtraRuns());
                        } else {
                            everyTeamCondactExtraRuns.put(delivery.getBattingTeam(), delivery.getExtraRuns());
                        }
                    }
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return everyTeamCondactExtraRuns;
    }


    public static List<Map.Entry<String, Float>> findTheMostEconomicalBowlerIn2015(List<Match> matches, List<Delivery> deliveries) {
        List<Map.Entry<String, Float>> sortEconomyBowler = null;
        
        try {
            HashMap<String, Float> economyBowlers = new HashMap<>();
            HashMap<String, Integer> legalDeliveries = new HashMap<>();
            HashMap<String, Integer> totaRuns = new HashMap<>();
            List<Integer> matchesId = new ArrayList<>();
            
            for (Match match : matches) {
                if (match.getSeason() == 2015) {
                    matchesId.add(match.getId());
                }
            }
            for (int i = 0; i < matchesId.size(); i++) {
                for (Delivery delivery : deliveries) {
                    if (matchesId.get(i) == delivery.getMatchId()) {
                        if (legalDeliveries.containsKey(delivery.getBowler()) && totaRuns.containsKey(delivery.getBowler())) {
                            if (delivery.getWideRuns() == 0 && delivery.getNoballRuns() == 0) {
                                legalDeliveries.put(delivery.getBowler(), legalDeliveries.get(delivery.getBowler()) + 1);
                            }
                            totaRuns.put(delivery.getBowler(), totaRuns.get(delivery.getBowler()) + delivery.getTotalRuns());
                        } else {
                            legalDeliveries.put(delivery.getBowler(), 1);
                            totaRuns.put(delivery.getBowler(), delivery.getTotalRuns());
                        }
                        economyBowlers.put(delivery.getBowler(), totaRuns.get(delivery.getBowler()) / (legalDeliveries.get(delivery.getBowler()) / 6f));
                    }
                }
            }

            sortEconomyBowler = new ArrayList<>(economyBowlers.entrySet());
            sortEconomyBowler.sort(Comparator.comparing(Map.Entry::getValue));

        }catch (Exception e){
            e.printStackTrace();
        }

        return sortEconomyBowler;
    }

    public static Map<String,Integer> findMatchesPlayedOnEveryVenue(List<Match> matches) {
        TreeMap<String, Integer> matchesPlayedOnVenue = new TreeMap<>();

        try {
            for (Match match : matches) {
                if (matchesPlayedOnVenue.containsKey(match.getVenue())) {
                    matchesPlayedOnVenue.put(match.getVenue(), matchesPlayedOnVenue.get(match.getVenue()) + 1);
                } else {
                    matchesPlayedOnVenue.put(match.getVenue(), 1);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return matchesPlayedOnVenue;
    }
    
    public static List<Match> getMatchesData() {
        List<Match> matches = new ArrayList<>();
        String path = "src/com/mayank/ipl/CSV_DataFile/matches.csv";

        try {
            BufferedReader bm = new BufferedReader(new FileReader(path));
            bm.readLine();
            String line = "";
            while ((line = bm.readLine()) != null) {
                String data[] = line.split(",");
                Match matchs = new Match();

                matchs.setId(Integer.parseInt(data[ID]));
                matchs.setSeason(Integer.parseInt(data[SEASON]));
                matchs.setCity(data[CITY]);
                matchs.setDate(data[DATE]);
                matchs.setTeam1(data[TEAM_1]);
                matchs.setTeam2(data[TEAM_2]);
                matchs.setTossWinner(data[TOSS_WINNER]);
                matchs.setTossDecision(data[TOSS_DECISION]);
                matchs.setResult(data[RESULT]);
                matchs.setDlApplied(Integer.parseInt(data[DL_APPLIED]));
                matchs.setWinner(data[WINNER]);
                matchs.setWinByRuns(Integer.parseInt(data[WIN_BY_RUNS]));
                matchs.setWinByWickets(Integer.parseInt(data[WIN_BY_WICKETS]));
                matchs.setPlayerOfMatch(data[PLAYER_OF_MATCH]);
                matchs.setVenue(data[VENUE]);
                if (data.length > 15) {
                    matchs.setUmpire1(data[UMPIRE_1]);}
                if (data.length > 16){
                    matchs.setUmpire2(data[UMPIRE_2]);}
                if (data.length > 17) {
                    matchs.setUmpire3(data[UMPIRE_3]);}

                matches.add(matchs);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matches;
    }

    public static List<Delivery> getDeliveriesData() {
        String path = "src/com/mayank/ipl/CSV_DataFile/deliveries.csv";
        List<Delivery> deliveries = new ArrayList<>();

        try {
            BufferedReader bd = new BufferedReader(new FileReader(path));
            bd.readLine();
            String line = "";
            while ((line = bd.readLine()) != null) {
                String data[] = line.split(",");
                Delivery delivery = new Delivery();
                delivery.setMatchId(Integer.parseInt(data[MATCH_ID]));
                delivery.setInning(Integer.parseInt(data[INNING]));
                delivery.setBattingTeam(data[BATTING_TEAM]);
                delivery.setBowlingTeam(data[BOWLING_TEAM]);
                delivery.setOver(Integer.parseInt(data[OVER]));
                delivery.setBall(Integer.parseInt(data[BALL]));
                delivery.setBatsman(data[BATSMAN]);
                delivery.setNonStriker(data[NON_STRIKER]);
                delivery.setBowler(data[BOWLER]);
                delivery.setIsSuperOver(Integer.parseInt(data[IS_SUPER_OVER]));
                delivery.setWideRuns(Integer.parseInt(data[WIDE_RUNS]));
                delivery.setByeRuns(Integer.parseInt(data[BYE_RUNS]));
                delivery.setLegbyeRuns(Integer.parseInt(data[LEGBYE_RUNS]));
                delivery.setNoballRuns(Integer.parseInt(data[NO_BALL_RUNS]));
                delivery.setPenaltyRuns(Integer.parseInt(data[PANELTY_RUNS]));
                delivery.setBatsmanRuns(Integer.parseInt(data[BATSMAN_RUNS]));
                delivery.setExtraRuns(Integer.parseInt(data[EXTRA_RUNS]));
                delivery.setTotalRuns(Integer.parseInt(data[TOTAL_RUNS]));
                if (data.length > 18){
                    delivery.setPlayerDismissed(data[PLAYER_DISMISSED]);}
                if (data.length > 19){
                    delivery.setDismissalKind(data[DISMISSAL_KIND]);}
                if (data.length > 20){
                    delivery.setFielder(data[FIELDER]);}

                deliveries.add(delivery);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveries;
    }

}