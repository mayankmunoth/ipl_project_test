package com.mayank.ipl;

import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class TestMain {

    static Main main;
    @BeforeClass
   public static void objectCreation(){
        main = new Main();
   }

   @Before
   public void beforeEachTest() {
       System.out.println("Test started");
   }

    @Test
    public void testFindNumberOfMatchesPlayed() {
        List<Match> match = new ArrayList<>();

        Match match1 = new Match();
        match1.setSeason(2010);
        match.add(match1);

        Match match2 = new Match();
        match2.setSeason(2010);
        match.add(match2);

        Match match3 = new Match();
        match3.setSeason(2011);
        match.add(match3);

        Match match4 = new Match();
        match4.setSeason(2012);
        match.add(match4);

        Match match5 = new Match();
        match5.setSeason(2011);
        match.add(match5);

       Map<Integer,Integer> noOfMatchesPlayed = new TreeMap<>();
        noOfMatchesPlayed.put(2010,2);
        noOfMatchesPlayed.put(2011,2);
        noOfMatchesPlayed.put(2012,1);

        assertEquals(noOfMatchesPlayed,main.findNumberOfMatchesPlayed(match));
        assertTrue(!main.findNumberOfMatchesPlayed(match).isEmpty());
        System.out.println("Test1 executed successfully...");
    }

   @Test
    public void testfindNumberOfMatchesWonPerTeam(){
        List<Match> matches = new ArrayList<>();

        Match match1 = new Match();
        match1.setWinner("CSK");
        matches.add(match1);

       Match match2 = new Match();
       match2.setWinner("MI");
       matches.add(match2);

       Match match3 = new Match();
       match3.setWinner("CSK");
       matches.add(match3);

       Match match4 = new Match();
       match4.setWinner("RR");
       matches.add(match4);

       Map<String,Integer> totalMatchesWon = new TreeMap<>();
       totalMatchesWon.put("CSK",2);
       totalMatchesWon.put("RR",1);
       totalMatchesWon.put("MI",1);

       assertEquals(totalMatchesWon,main.findNumberOfMatchesWonPerTeam(matches));
       assertFalse(main.findNumberOfMatchesWonPerTeam(matches).isEmpty());
       System.out.println("Test2 executed successfully...");
    }

    @Test
    public void TestFindExtraRunsConcededPerTeamIn2016(){
        List<Match> matches = new ArrayList<>();
        List<Delivery> deliveries = new ArrayList<>();

        Match match1 = new Match();
        match1.setId(1);
        match1.setSeason(2016);
        matches.add(match1);

        Match match2 = new Match();
        match2.setId(2);
        match2.setSeason(2015);
        matches.add(match1);

        Match match3 = new Match();
        match3.setId(3);
        match3.setSeason(2016);
        matches.add(match3);

        Delivery delivery1 = new Delivery();
        delivery1.setMatchId(1);
        delivery1.setBattingTeam("CSK");
        delivery1.setExtraRuns(10);
        deliveries.add(delivery1);

        Delivery delivery2 = new Delivery();
        delivery2.setMatchId(2);
        delivery2.setBattingTeam("MI");
        delivery2.setExtraRuns(5);
        deliveries.add(delivery2);

        Delivery delivery3 = new Delivery();
        delivery3.setMatchId(3);
        delivery3.setBattingTeam("RR");
        delivery3.setExtraRuns(8);
        deliveries.add(delivery3);

        Map<String,Integer> extraRunConductEveryTeam = new TreeMap<>();
        extraRunConductEveryTeam.put("CSK",20);
        extraRunConductEveryTeam.put("RR",8);
        deliveries.add(delivery2);

        assertEquals(extraRunConductEveryTeam,main.findExtraRunsConcededPerTeamIn2016(matches,deliveries));
        assertNotNull(main.findExtraRunsConcededPerTeamIn2016(matches,deliveries));
        assertFalse(main.findExtraRunsConcededPerTeamIn2016(matches,deliveries).isEmpty());
        System.out.println("Test3 executed successfully...");
    }

    @Test
    public void TestFindTheMostEconomicalBowlerIn2015(){
        List<Match> matches = new ArrayList<>();
        List<Delivery> deliveries = new ArrayList<>();

        Match match1 = new Match();
        match1.setId(1);
        match1.setSeason(2015);
        matches.add(match1);

        Match match2 = new Match();
        match2.setId(2);
        match2.setSeason(2016);
        matches.add(match2);

        Match match3 = new Match();
        match3.setId(3);
        match3.setSeason(2015);
        matches.add(match3);

        Delivery delivery1 = new Delivery();
        delivery1.setMatchId(1);
        delivery1.setBowler("A");
        delivery1.setWideRuns(0);
        delivery1.setNoballRuns(0);
        delivery1.setTotalRuns(1);
        deliveries.add(delivery1);

        Delivery delivery2 = new Delivery();
        delivery2.setMatchId(1);
        delivery2.setBowler("A");
        delivery2.setWideRuns(0);
        delivery2.setNoballRuns(0);
        delivery2.setTotalRuns(1);
        deliveries.add(delivery2);

        Delivery delivery3 = new Delivery();
        delivery3.setMatchId(2);
        delivery3.setBowler("B");
        delivery3.setWideRuns(0);
        delivery3.setNoballRuns(0);
        delivery3.setTotalRuns(1);
        deliveries.add(delivery3);

        Delivery delivery4 = new Delivery();
        delivery4.setMatchId(3);
        delivery4.setBowler("C");
        delivery4.setWideRuns(0);
        delivery4.setNoballRuns(0);
        delivery4.setTotalRuns(4);
        deliveries.add(delivery4);

        Delivery delivery5 = new Delivery();
        delivery5.setMatchId(3);
        delivery5.setBowler("C");
        delivery5.setWideRuns(0);
        delivery5.setNoballRuns(0);
        delivery5.setTotalRuns(6);
        deliveries.add(delivery5);

        Delivery delivery6 = new Delivery();
        delivery6.setMatchId(1);
        delivery6.setBowler("C");
        delivery6.setWideRuns(5);
        delivery6.setNoballRuns(0);
        delivery6.setTotalRuns(0);
        deliveries.add(delivery6);

         Map<String,Float> mostEconomyPlayer = new TreeMap<>();
         mostEconomyPlayer.put("A", 6.0F);
         mostEconomyPlayer.put("C",20.0F);

        List<Map.Entry<String, Float>> sortEconomyBowler= new ArrayList<>(mostEconomyPlayer.entrySet());
        sortEconomyBowler.sort(Comparator.comparing(Map.Entry::getValue));

        assertEquals(sortEconomyBowler,main.findTheMostEconomicalBowlerIn2015(matches,deliveries));
        Assert.assertNotNull(main.findTheMostEconomicalBowlerIn2015(matches,deliveries));
        System.out.println("Test4 executed successfully...");
    }

     @Test
    public void testFindMatchesPlayedOnEveryVenue(){

        List<Match> match =new ArrayList<>();
        Match match1 =new Match();
        match1.setVenue("SMS");
        match.add(match1);

        Match match2 =new Match();
        match2.setVenue("SMS");
        match.add(match2);

        Match match3 =new Match();
        match3.setVenue("CS");
        match.add(match3);

        Map<String, Integer> totalMatchPlayedVenue = new TreeMap<>();
        totalMatchPlayedVenue.put("CS",1);
        totalMatchPlayedVenue.put("SMS",2);

        assertEquals(totalMatchPlayedVenue,main.findMatchesPlayedOnEveryVenue(match));
        assertNotNull(main.findMatchesPlayedOnEveryVenue(match));
        System.out.println("Test5 executed successfully...");

    }

    @Test(expected = NullPointerException.class)
    public void checkException() {
        System.out.println(" NO Excettion");
    }

    @Test(timeout = 1)
    public void checktimeLimit()
    {
        System.out.println("successully executed within time");
    }

    @After
    public void AfterEachTest() {
        System.out.println("Test ended");
    }

}
